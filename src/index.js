import 'es5-shim'
import 'babel-polyfill'

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import injectTapEventPlugin from 'react-tap-event-plugin'

import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import logger from './middlewares/logger'
import {add, edit, remove, toggle} from './actions/todos'
import TodoApp from './containers/todoApp'
import DevTools from './containers/devtools';
import todoApp from './reducers'

injectTapEventPlugin()

const createStoreWithMiddleware = compose(
  applyMiddleware(logger, thunk),
  DevTools.instrument()
)(createStore)

const store = createStoreWithMiddleware(todoApp)

render(
  <Provider store={store}>
    <div>
      <TodoApp />
      <DevTools />
    </div>
  </Provider>,
  document.getElementById('root')
);
