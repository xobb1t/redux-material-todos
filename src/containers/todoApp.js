import React, {Component, PropTypes} from 'react'
import { connect } from 'react-redux';

import ListDivider from 'material-ui/lib/lists/list-divider'

const Toolbar = require('material-ui/lib/toolbar/toolbar');
const ToolbarGroup = require('material-ui/lib/toolbar/toolbar-group');
const ToolbarSeparator = require('material-ui/lib/toolbar/toolbar-separator');
const ToolbarTitle = require('material-ui/lib/toolbar/toolbar-title');
const RaisedButton = require('material-ui/lib/raised-button');

import {add, remove, toggle} from '../actions/todos'
import {toggleVisibility} from '../actions/visibility_filter'
import {openDialog, closeDialog} from '../actions/ui'

import AddTodoDialog from '../components/addTodo'
import TodoItems from '../components/todoItem'

const containerStyle = {
  paddingTop: 50,
  width: '50%',
  margin: '0 auto'
};

class TodoApp extends Component {
  static propTypes = {
    add: PropTypes.func.isRequired,
  }

  static contextTypes = {
    store: PropTypes.object.isRequired
  }
  _handleAdd(value) {
    const {add, closeDialog} = this.props
    add(value)
    closeDialog()
  }
  render() {
    return (
      <div style={containerStyle}>
        <Toolbar>
          <ToolbarGroup float="left">
            <RaisedButton label="Create new" primary={true} onTouchTap={this.props.openDialog}/>
          </ToolbarGroup>
        </Toolbar>
        {this.props.todosInProgress.length !== 0 &&
          <TodoItems onClick={this.props.toggle} onRemove={this.props.remove} items={this.props.todosInProgress} subheader="In progress"/>
        }
        {
          this.props.todosInProgress.length !== 0 &&
          this.props.completedTodos.length !== 0 &&
          <ListDivider/>
        }
        {this.props.completedTodos.length !== 0 &&
          <TodoItems onClick={this.props.toggle} onRemove={this.props.remove} items={this.props.completedTodos} subheader="Completed"/>
        }
        <AddTodoDialog onAdd={this._handleAdd.bind(this)} onCancel={this.props.closeDialog} open={this.props.ui.newTodoOpen}/>
      </div>
    )
  }
}

const stateLense = state => {
  let {todos, ui} = state;
  const completedTodos = todos.filter(todo => todo.completed)
  const todosInProgress = todos.filter(todo => !todo.completed)
  return {completedTodos, todosInProgress, ui}
}

export default connect(
  stateLense,
  {add, remove, toggle, openDialog, closeDialog},
)(TodoApp)
