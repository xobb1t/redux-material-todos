import { ADD_TODO, EDIT_TODO, REMOVE_TODO, TOGGLE_TODO } from '../constants/todos'

export function add(text) {
  return {
    type: ADD_TODO,
    text
  }
}

export function remove(id) {
  return {
    type: REMOVE_TODO,
    id
  }
}

export function edit(id, text) {
  return {
    type: EDIT_TODO,
    id,
    text
  }
}

export function toggle(id) {
  return {
    type: TOGGLE_TODO,
    id
  }
}
