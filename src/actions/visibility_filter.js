import {TOGGLE_VISIBILITY} from '../constants/visibility_filter'


export function toggleVisibility() {
  return {
    type: TOGGLE_VISIBILITY
  }
}
