import React, {Component, PropTypes} from 'react'

import IconButton from 'material-ui/lib/icon-button'
import FontIcon from 'material-ui/lib/font-icon'
import List from 'material-ui/lib/lists/list'
import ListItem from 'material-ui/lib/lists/list-item'

const TodoItem = ({item, onClick, onRemove}) => {
  return (
    <ListItem
      key={item.id}
      value={item.id}
      leftIcon={item.completed && <FontIcon className="material-icons">done</FontIcon>}
      rightIconButton={(
        <IconButton onTouchTap={() => onRemove(item.id)}>
          <FontIcon className="material-icons">delete</FontIcon>
        </IconButton>
      )}
      primaryText={item.text}
      onTouchTap={() => onClick(item.id)}/>
  )
}

export default function ({items, onClick, onRemove, subheader}) {
  return (
    <List subheader={subheader}>
      {items.map(item => TodoItem({item, onClick, onRemove}))}
    </List>
  )
}
