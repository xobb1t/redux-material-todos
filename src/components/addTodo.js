import React, {Component, PropTypes} from 'react'

import Dialog from 'material-ui/lib/dialog'
import TextField from 'material-ui/lib/text-field'

export default class AddTodo extends Component {
  render() {
    const dialogActions = [
      { text: 'Cancel', onTouchTab: this.props.onCancel},
      { text: 'Add', onTouchTap: this._handleSubmit.bind(this), ref: 'submit' }
    ]

    return (
      <Dialog
        title="Create new todo"
        actions={dialogActions}
        actionFocus="submit"
        open={this.props.open}
        onRequestClose={this.props.onCancel}>
          <TextField ref="newTodoField" hintText="New todo text" onEnterKeyDown={this._handleSubmit.bind(this)}/>
      </Dialog>
    )
  }
  _handleSubmit() {
    const node = this.refs.newTodoField
    const text = node.getValue()
    if (text) {
      node.setValue('');
      this.props.onAdd(text)
    } else {
      node.setErrorText('Enter new todo text!')
    }
  }
}

AddTodo.propTypes = {
  onAddClick: PropTypes.func.isRequired
}
