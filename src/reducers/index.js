import _ from 'lodash'
import { combineReducers } from 'redux'
import {ADD_TODO, EDIT_TODO, REMOVE_TODO, TOGGLE_TODO} from '../constants/todos'
import {OPEN_DIALOG, CLOSE_DIALOG} from '../constants/ui'
import {TOGGLE_VISIBILITY} from '../constants/visibility_filter'

function visibilityFilter(state = false, action) {
  switch (action.type) {
    case TOGGLE_VISIBILITY:
      return !state
    default:
      return state
  }
}

function ui(state = {newTodoOpen: false}, action) {
  const reducers = {
    [OPEN_DIALOG]() {
      return {
        ...state,
        newTodoOpen: true
      }
    },
    [CLOSE_DIALOG]() {
      return {
        ...state,
        newTodoOpen: false
      }
    }
  }
  return _.get(reducers, action.type, () => state)()
}

const initialTodos = [
  {
    id: 1,
    text: 'Learn redux',
    completed: false
  },
  {
    id: 2,
    text: 'Launch todos',
    completed: true
  }
]

function todos(state = initialTodos, action) {
  const reducers = {
    [ADD_TODO]() {
      return [
        ...state,
        {
          id: state.reduce((maxId, todo) => Math.max(todo.id, maxId), 0) + 1,
          text: action.text,
          completed: false
        }
      ]
    },
    [EDIT_TODO]() {
      return state.map(todo =>
        todo.id === action.id ? {...todo, text: action.text} : todo
      )
    },
    [REMOVE_TODO]() {
      return state.filter(todo => todo.id !== action.id)
    },
    [TOGGLE_TODO]() {
      return state.map(todo =>
        todo.id === action.id ? {...todo, completed: !todo.completed} : todo
      )
    }
  }
  return _.get(reducers, action.type, () => state)()
}

export default combineReducers({
  visibilityFilter,
  todos,
  ui
})
