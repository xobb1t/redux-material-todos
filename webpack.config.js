var path = require('path')

module.exports = {
  // devtool: 'cheap-module-eval-source-map',
  entry: './src/index',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/',
    sourceMapFilename: '[file].map'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        include: __dirname,
        loaders: [ 'babel-loader' ],
      },
      {
        test: /\.(jpe?g|png|gif)$/,
        loader: 'file-loader?name=images/[name].[hash].[ext]'
      },
      {
        test: /\.(eot|ttf|ijmap|woff|woff2)$/,
        loader: 'file-loader?name=fonts/[name].[hash].[ext]'
      },
    ],
    resolve: {
      extensions: ['', '.js', '.jsx']
    }
  },
  devtool: 'source-map',
  plugins: [
    // new webpack.optimize.UglifyJsPlugin({minimize: true})
  ],
}
